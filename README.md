<div align="center"><h1>简搭云可视化表单设计</h1></div>
<div align="center"><h3>有可能是扩展性最好的表单设计器之一</h3></div>

<div align="center">

[![star](https://gitee.com/liuyaping007/vuefrom1.1.0/badge/star.svg?theme=dark)](https://gitee.com/liuyaping007/vuefrom1.1.0/stargazers)
[![fork](https://gitee.com/liuyaping007/vuefrom1.1.0/badge/fork.svg?theme=dark)](https://gitee.com/liuyaping007/vuefrom1.1.0/members)
[![GitHub license](https://img.shields.io/badge/license-Apache2-yellow)](https://gitee.com/dotnetchina/Furion/blob/master/LICENSE)

</div>
# 🏆相关文档

仓库代码地址：

【 [码云仓库](https://gitee.com/liuyaping007/vuefrom1.1.0)】

【 [文档地址](http://qifeng.321zou.com)】

【 [体验地址](http://kyform.cn/) 用户名：admin 密码：admin123】

#### 🏆使用技术点：
vue2.0  element-ui  monaco vuedraggable jsplumb.js jquery jquerytemplate等插件

#### 🏆安装教程 
nodejs其他版本下载地址：
[输入链接说明](https://nodejs.org/zh-cn/download/releases/)

- 首次下载项目后，安装项目依赖：

```
npm install
```

- 本地开发

```
npm run serve
```

- 构建

```
npm run build
```
#### 🏆简搭云可视化表单特性与介绍。
简搭云可视化表单最大的优势是直接vue代码直接渲染，而非JSON数组性渲染，市面上大部分vue可视化表单都是json渲染，vue源码渲染具有更好的高类聚，低耦合，复用性高，封装性强，易扩展的等特点。因为vue源码直接渲染模式，只要在线编写的代码和生成vue代码符合vue语法风格，就能进行渲染，无需编译，充分继承了vue强大的扩展性，可读性，聚合性，复用性，封装性等特点，他也可以很轻松集成市面上所有vue组件。如echart，datav，饿了么UI，百度地图，高德地图，antUI，各种自定义组件等。

与市面上表单不一样的特点有：

##### 🚀1.一套表单三种模式一起设计

##### 🚀2.生成一套代码同时适应PC，ipad，手机端的预览。

##### 🚀3.支持在线编辑vue代码逻辑，不与可视化设计器生成的vue代码进行冲突。也可支持输入debugger，进行设计同时进行调试。

##### 🚀4.支持css代码在线编写，可针对表单风格进行编写css。

##### 🚀5.可下载vue源码到本地，无需更改任何更改，可直接预览。

##### 🚀6.属性，组件，事件，通用css都可以通过配置文件进行配置。

##### 🚀7.vue源码生成采用jquery+jquerytemplate模板生成，根据不同业务，框架生成不同风格，业务，框架的代码，如目前已有项目适应的表单有：element-ui表单，ant-ui表单，mini-ui的表单，同样也可以支持也可以生成appui，小程序语法代码等。

##### 🚀8.根据业务制定不同模板，生成不同的业务表单：如问卷调查表单，可视化大屏，流程表单，通用增删改查表单，只要业务存在一定的通用性，我们就可以创建一个不同的模板解析

##### 🚀9.支持很多快捷的方式，如点击事件，可切换源码，编辑对应事件的方法。

##### 🚀10.明细组件拖拽完成自动支持导入，导出。

##### 🚀11.增加行列控件，能更好的精确布局。

#### 🏆简搭云可视化表单实现无码开发设计运行思维图

![可视化表单实现无码开发设计运行思维图](https://oscimg.oschina.net/oscnet/up-b46c4c81fb54d517a1f1eee35adcf84b6b0.png)

由思维图中可以看出可视化平台主要由两个部分构成：表单设计器与mybatis语法解析引擎构成。而生成的代码由jquerytemplate 模板语法生成，所以可以支持element-ui框架，mini——ui框架，ant design框架，uniapp框架的源码生成，但是需要如下解决红色文字问题

![](https://oscimg.oschina.net/oscnet/up-2672a6b5ea59913ac681681f64b76e99fb1.png)

好了废话少说，直接上图

| pc端设计                                                             | idad端设计                                                                  | 手机端设计                                                               |
|------------------------------------------------------------------|------------------------------------------------------------------------|------------------------------------------------------------------------|
|![pc端设计](https://oscimg.oschina.net/oscnet/up-03a37ba990272a03c333a6df9f3190799b4.png) | ![idad端设计](https://oscimg.oschina.net/oscnet/up-d1f75bae1096060b6eba0e2224eada9232c.png) | ![手机端设计](https://oscimg.oschina.net/oscnet/up-5547906ea98c953f3b7cce836b1eaace0d9.png) |
| pc端预览                                                             | idad端预览                                                                  | 手机端预览                                                               |
|![pc端设计](https://oscimg.oschina.net/oscnet/up-807c0b90c6e5f7426d0ecdfdd26064ec392.png) | ![idad端设计](https://oscimg.oschina.net/oscnet/up-cf7bbbc9abf508e07c714446115a810f766.png) | ![手机端设计](https://oscimg.oschina.net/oscnet/up-850b67b070e3d92c02a96bef46b01447aae.png) |
|查看源码js部分 ，可在线编辑代码，可添加函数,与设计生成代码进行融合                                                             | template html部分                                                                 | css部分，可根据界面风格进行调整,自行调整|
|![pc端设计](https://oscimg.oschina.net/oscnet/up-06a3e616ed876f70ea4de8601188493c1b9.png) | ![idad端设计](https://oscimg.oschina.net/oscnet/up-1bf3fb915cf8c1075db970e3981e438fe29.png) | ![手机端设计](https://oscimg.oschina.net/oscnet/up-f585b9022d460f9a602ddd87b0bea7e5722.png) |

导出vue源码，直接拷贝到项目中可直接使用，配置路由后就可以访问。

![输入图片说明](https://oscimg.oschina.net/oscnet/up-a7886b8ee1945b0b6f6b92e4856fd8f5c76.png)

#### 🏆Mybatis动态接口

在线动态接口,保存后即可生成增，删，查，导出，导入的动态接口，也可以编辑修改动态接口
![输入图片说明](https://oscimg.oschina.net/oscnet/up-dcbef0b3cc67211b5b26bbf6946bc9a8dc1.png)
动态接口

| 接口编辑，可智能提示表名，表字段  | 接口参数后端验证                                                            | 列表接口字段显示                                                               |接口在线测试|
|------------------------------------------------------------------|------------------------------------------------------------------------|------------------------------------------------------------------------|------------------------------------------------------------------------|
![输入图片说明](https://oscimg.oschina.net/oscnet/up-3a7a86178d54d6d29fb2e0a529f27d37e40.png)|![输入图片说明](https://oscimg.oschina.net/oscnet/up-fb89d3ef582fa8cc84f36875e129a1b4db4.png)|![输入图片说明](https://oscimg.oschina.net/oscnet/up-d9be34d94b03bccaadf97840a83d7a40ac7.png)|![输入图片说明](https://oscimg.oschina.net/oscnet/up-0440ce3ad30d84fab9e15aba3f0d2bc075f.png)

#### 🏆动态接口使用方法说明
Mybatis语法是个不错的语法，能够将实体与数据库紧密的结合在一起，减少了存贮过程，视图的编写，而且比存贮过程，视图更多元化，更强大,有人说Mybatis已经很成熟了，为什么自己还需要开发一套这个组件，第一：主要是因为Mybatis语法只能写在Mapping和Dao实体中，写在这里就必须要编译。第二：传入的Map实体不能是解析的。

我的Mybatis语法引擎能够动态解析Json实体，获取其属性值进行拼装SQL，那我将Mybatis语法配置提取到前端网页中，保存到数据库中，设置好两者约定好提交json实体结构， 然后封装一个通用Api接口提供调用，通过请求参数找到Mybatis配置信息，提交约定好的Json实体结构的数据，从而实现对数据库的各交互的一个万能接口。该模式去掉了我们平时开发的实体层，Service层，Dao层，能够实现60%的后端业务功能，配置好接口就可以立马使用，同时也解决了无码开发后端的编码问题。遇到复杂的逻辑需二开，也支持后端java代码的调用。

Mybatis语法引擎设计思路就是参照了Mybatis底层实现原理实现的，在编写时在Mybatis sql语法中约定提交的实体，约定的实体可以是任何JSON
通过#{属性名}映射获取json的属性值.例如:{bb:"323",cc:"434",fff:'fdsf'} 配置获取bb的值，如：#{bb} 获取cc的值,如：#{cc}
通过#{属性名.子属性名称}映射获取属性对应子属性值.例如:提交json为：{bb:{cc:"fdsfds"}} 想获取cc的值#{bb.cc}
通过#{属性名[0].子属性}映射获取数组属性，第一行的子属性值。例如:提交json为：{bb:[{cc:'dsad'},{cc:'324'},{cc:'434'},{cc:'343'}]} 获取bb第一行cc的值，如:#{bb[0].cc}
通过<for forname='属性名'>#{属性名[$index0].子属性} </for> 通过循坏获取子属性数组中具体的子属性.例如：提交json为{bb:[{cc:'dsad'},{cc:'324'},{cc:'434'},{cc:'343'}]} 选循坏获取所有的cc <for forname='bb'> select *from table where column=#{bb[$index0].cc} </for>
<sql param='ccc' > select * from AA </sql> 将sql语句查询的结果放入JSON实体对象属性ccc中，后面sql脚本就可以用#{ccc[0].子属性} 并做参数入值。
<sql test='ccc!=0 and ccc>5' >select * from AA</sql > test属性也支持条件判断
也支持#{A属性+B属性}，#{A属性*B属性}，#{A属性/B属性}，#{A属性-B属性}，#{A属性==''?B属性:A属性} 最基础的运算与三元表达式的支持。
编写的Mybatis语法代码不是保存在xml中，而是保存在在数据库中，随时更改，随时生效。
结合当前环境变量#{$user}当前登录对象，#{$user.userId} #{$user.userName} #{$user.realaname} #{$user.deptcode} #{$user.deptname} #{$user.organcode} #{$user.organname} #{$user.deptid} #{$user.organid} #{$user.password} #{$user.phone} #{$user.avatar}

##### 🏆优点
###### 🚀1.可以节省了开发人员的各种实体编写，不同层级之间的调用，直接编写mybastis语法，目标功能实现，效率能够大大提升。

###### 🚀2.可根据需求变化而变化，灵活更改对应接口，灵活性非常好

###### 🚀3.后期运维，逻辑简单，响应速度非常快。

###### 🚀5.迁移与重复非常方便，因为整个配置只有一张表，只要将一条数据迁移过去，功能就迁移过去了，与数据库的藕合性低。

###### 🚀6.可以与java紧密结合起来，（前期使用配置开发，发现后期需求变变化到无法满足的地步，就需要二次开发，可在java代码中调用。）

##### 🏆扩展思维
###### 🚀1. 系统之间接口的对接，接口返回的json格式是固定的，如何将接口数据对接我们系统中，结合定时任务的配置是可以完美解决，各种系统数据对接和输出，实现万能对接接口
###### 🚀2. MQ队列，MQ队列接收也是实体JSON格式，也是可以完美解决各种数据的同步与数据处理入库
###### 🚀3. 各种xls的导入，导入的模板固定，对应JSON格式也就固定，同样可以适应，实现万能数据导入、导出接口
###### 🚀4. 各种Api接口的开发，Api接口提交JSON格式数据是固定的，如现在系统的各种功能开发就用到了该接口。
###### 🚀5. 因为接口是一个组件，也可以支持各种接口微服务，服务器负载均衡部署。

#### 🏆流程平台
流程平台与表单引擎进行了很好融合，每个节点可以控制表单控件的只读，必填，显示，审批人可以是具体人员，角色，机构部门，也可以是表单控件值。
路由支持表单数据为条件控制流程走向，业务数据会自动保存，也可以每个节点进行设置回调Api和执行的动态Mybatis语法接口，表单设计器如下：

![](https://oscimg.oschina.net/oscnet/up-c0fd0a0ee2c7abdd0ff6c7d3f31385cfc52.png)

流程发起PC端

| 流程信息  | 审批记录                                                            | 流程走向                                                               |
|------------------------------------------------------------------|------------------------------------------------------------------------|------------------------------------------------------------------------|
![输入图片说明](https://oscimg.oschina.net/oscnet/up-ea3510e28df6efb834fb07f6957f0478108.png)|![输入图片说明](https://oscimg.oschina.net/oscnet/up-10720a4757018cf193fe81aac3a3ac16a1c.png)|![输入图片说明](https://oscimg.oschina.net/oscnet/up-f3beb490f62be80e1d65d7e3498e31b3a1f.png)

流程发起手机端

| 流程信息  | 审批记录                                                            |
|------------------------------------------------------------------|------------------------------------------------------------------------|
![输入图片说明](https://oscimg.oschina.net/oscnet/up-d209fcb5a46ba98e96227e036048469d638.png)|![输入图片说明](https://oscimg.oschina.net/oscnet/up-6592df3b416cb4e38eb237b34d3dd29d0fe.png)|

## 🏆交流
- QQ群 109434403
- qq联系人：329175905
微信号： 18670793619
扫一扫加好友

| 微信                                                              | qq                                                                    | qq群                                                                    |
|------------------------------------------------------------------|------------------------------------------------------------------------|------------------------------------------------------------------------|
|<img src="https://oscimg.oschina.net/oscnet/up-582fc3f0b0152879dffdc0e9302b85e8a95.png" width="300" height="180px"  alt="微信"/><br/> | ![](https://oscimg.oschina.net/oscnet/up-933f0fcf9c91152aa625b06ee857f11d3f2.png) | ![](https://oscimg.oschina.net/oscnet/up-3688b49bd44df9757c3c6162d36b1f15a69.png) |
